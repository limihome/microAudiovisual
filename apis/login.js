import {
	request
} from '@/utils/request'

export function doLogin(data) {
	return request.post('/rest/user/login', data);
}

export function doGetSmsCode(data) {
	return request.post('/rest/user/login', data);
}

export function doRegist(data) {
	return request.post('/rest/user/register', data);
}

export function doUpdatePwd(data) {
	return request.post('/rest/user/update', data);
}

export function doLogout() {
	return request({
		url: '/rest/user/login',
		method: 'post',
		data: data, //请求参数放请求体里
		headers: {
			'Content-type': 'application/json'
		}
	})
}

export function doSmsCode(params) {
	return request.post('/rest/user/login', data);
}

export function fetchOrderData() {
	return request.post('/rest/user/login', data);
}

import {
	request
} from '@/utils/request'

export function doFetchList(data) {
	return request.post('/rest/address/list', data);
}

export function doDelete(data) {
	return request.get('/rest/address/delete/' + data.id);
}

export function doInsert(data) {
	return request.post('/rest/address/add/', data);
}

export function doUpdate(data) {
	return request.post('/rest/address/update/', data);
}

export const appConfig = {
    tokenKey: 'x-access-token',
    // apiUrl: process.env.NODE_ENV === 'development' ? 'http://127.0.0.1:2001' : '',
	//环境标识
	NODE_ENV: 'production',
	// IP地址
	baseURL: 'http://127.0.0.1:2001',
	apiUrl: 'http://127.0.0.1:2001',
	// 请求超时时间
	timeout: 20000, // 不可超过 manifest.json 中配置 networkTimeout的超时时间
	// #ifdef H5
	withCredentials: true,
	// #endif
	// 跨域是否带token
	withCredentials: true,
	headers: {
		// 'Content-Type': 'application/x-www-form-urlencoded',
		'Content-Type': 'application/json'
	}
}
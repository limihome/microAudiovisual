import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

/**
 * 这里定义的方法，第一个参数必须是state,第二个参数才是函数需要的参数
 */
const store = new Vuex.Store({
	state: {
		/**
		 * 是否需要强制登录
		 */
		forcedLogin: false,
		/**
		 * 是否已经登录
		 */
		hasLogin: false,
		userInfo: {},
	},
	mutations: {
		login(state, provider) {
			// uni.clearStorageSync();
			uni.removeStorage({
				key: 'userInfo'
			})
			uni.removeStorage({
				key: 'accessToken'
			})
			state.hasLogin = true;
			state.userInfo = provider.member;
			uni.setStorage({ //缓存用户登陆状态
				key: 'user',
				data: provider
			});
			uni.setStorage({ //缓存用户登陆状态
				key: 'loginTime',
				data: new Date().getTime() / 1000
			});
			uni.setStorage({ //缓存用户登陆状态
				key: 'userInfo',
				data: provider.member
			})
			uni.setStorage({ //缓存用户登陆状态
				key: 'accessToken',
				data: provider.access_token
			})
			uni.setStorage({ //缓存用户登陆状态
				key: 'refreshToken',
				data: provider.refresh_token
			})
		},
		logout(state) {
			state.hasLogin = false;
			state.userInfo = {};
			uni.removeStorage({
				key: 'userInfo'
			})
			uni.removeStorage({
				key: 'accessToken'
			})
		},
		// 统一跳转接口,拦截未登录路由
		navTo(state, url) {
			if (!url) {
				this.$api.msg('我还没写');
				return;
			}
			this.userInfo = uni.getStorageSync('userInfo') || {};
			if (!this.userInfo) {
				url = '/pages/public/login';
				uni.showModal({
					content: '你暂未登陆!!!\n\r是否跳转登录页面？',
					success: confirmRes => {
						if (confirmRes.confirm) {
							uni.navigateTo({
								url
							});
						}
					}
				});
			} else {
				uni.navigateTo({
					url
				});
			}
		}
	},
	actions: {}
})

export default store
